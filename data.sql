--
-- File generated with SQLiteStudio v3.4.3 on Sat Feb 18 16:32:06 2023
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: posts
DROP TABLE IF EXISTS posts;

CREATE TABLE IF NOT EXISTS posts (
    id             INTEGER       PRIMARY KEY ASC ON CONFLICT FAIL AUTOINCREMENT
                                 NOT NULL ON CONFLICT FAIL,
    title          VARCHAR (150) NOT NULL
                                 UNIQUE,
    html           TEXT          NOT NULL
                                 DEFAULT (''),
    markdown       TEXT          NOT NULL
                                 DEFAULT (''),
    tags           VARCHAR (500) NOT NULL
                                 DEFAULT (''),
    language       CHAR (2)      NOT NULL
                                 DEFAULT ES,
    slug           VARCHAR (350) NOT NULL
                                 DEFAULT (''),
    status         VARCHAR (15)  DEFAULT DRAFT
                                 CHECK (status IN ('DRAFT', 'PUBLISHED', 'SYNCHRONIZE') ) 
                                 NOT NULL,
    translation_id INT           REFERENCES translations (id) ON DELETE RESTRICT
                                                              ON UPDATE CASCADE
                                 NOT NULL
                                 DEFAULT (0),
    created_at     DATETIME      NOT NULL
                                 DEFAULT (DATETIME('NOW', 'LOCALTIME') ),
    updated_at     DATETIME      NOT NULL
                                 DEFAULT (DATETIME('NOW', 'LOCALTIME') ) 
);


-- Table: translations
DROP TABLE IF EXISTS translations;

CREATE TABLE IF NOT EXISTS translations (
    id    INTEGER PRIMARY KEY AUTOINCREMENT
                  NOT NULL,
    count INT     DEFAULT (0) 
                  NOT NULL
);


-- Trigger: after_ins_without_trans
DROP TRIGGER IF EXISTS after_ins_without_trans;
CREATE TRIGGER IF NOT EXISTS after_ins_without_trans
                       AFTER INSERT
                          ON posts
                    FOR EACH ROW
                        WHEN NEW.translation_id IS NULL OR 
                             NEW.translation_id < 1
BEGIN
    UPDATE posts
       SET created_at = DATETIME('NOW') 
     WHERE id = NEW.id;
    INSERT INTO translations (
                                 count
                             )
                             VALUES (
                                 1
                             );
    UPDATE posts
       SET translation_id = (
               SELECT MAX(id) 
                 FROM translations
           )
     WHERE id = NEW.id;
END;


-- Trigger: after_insert_with_trans
DROP TRIGGER IF EXISTS after_insert_with_trans;
CREATE TRIGGER IF NOT EXISTS after_insert_with_trans
                       AFTER INSERT
                          ON posts
                    FOR EACH ROW
                        WHEN NEW.translation_id IS NOT NULL AND 
                             NEW.translation_id > 0
BEGIN
    UPDATE posts
       SET created_at = DATETIME('NOW') 
     WHERE id = NEW.id;
    UPDATE translations
       SET count = count + 1
     WHERE id = NEW.translation_id;
END;


-- Trigger: after_upd
DROP TRIGGER IF EXISTS after_upd;
CREATE TRIGGER IF NOT EXISTS after_upd
                       AFTER UPDATE OF title,
                                       html,
                                       markdown,
                                       tags,
                                       language,
                                       slug,
                                       status,
                                       translation_id
                          ON posts
                    FOR EACH ROW
BEGIN
    UPDATE posts
       SET updated_at = DATETIME('NOW', 'LOCALTIME') 
     WHERE id = NEW.id;
END;


-- Trigger: before_upd
DROP TRIGGER IF EXISTS before_upd;
CREATE TRIGGER IF NOT EXISTS before_upd
                      BEFORE UPDATE OF translation_id
                          ON posts
                    FOR EACH ROW
                        WHEN OLD.translation_id <> NEW.translation_id
BEGIN
    UPDATE translations
       SET count = count - 1
     WHERE id = OLD.translation_id;
    UPDATE translations
       SET count = count + 1
     WHERE id = NEW.translation_id;
END;


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
