var express = require('express')
var router = express.Router()
const posts = new require('../db/Posts')
const showdown  = require('showdown')
const slugify = require('slugify')
const converter = new showdown.Converter({
  simplifiedAutoLink: true,
  openLinksInNewWindow: true
})

const fetchByPK = (pk, callback) => {
  posts.fetchByPK(['id', 'title', 'markdown', 'html', 'tags', 'status'], pk, callback)
}

const render = (res, err, row, partial) => {
  if (err) {
    res.render('partial/error.njk', { error: err })
  } else {
    res.render(`partial/${partial}.njk`, { selected: row });
  }  
}

router.get('/:id(\\d+)/:action(view|edit)', function(req, res) {
  let partial = req.params.action.toLowerCase()
  console.log(`Request action: ${partial} over post: ${req.params.id}`)
  fetchByPK(req.params.id, function (err, row) {
    render(res, err, row, partial)
  })
});

router.get('/new', function(_, res) {
  res.render(`partial/edit.njk`);
})

router.get('/:id(\\d+)/translate', function(req, res) {
  res.render('partial/edit.njk', { selected: { translation_id: req.params.id } });
})

router.put('/', function(req, res) {
  let post = req.body
  if (post.markdown) {
    post.html =  converter.makeHtml(post.markdown)
  }
 
  let result = {
    op: 'UPD' 
  }

  post.slug = slugify(post.title, {
    lower: true,      // convert to lower case, defaults to `false`
    strict: true
  })

  posts.update(post, function (err) {
    if (err) {
      result.status = 'ERR'
      result.err = err
    } else {
      result.status = 'OK'
      result.changes = this.changes
    }
    res.json(result)
  })
})

router.post('/', function(req, res) {
  let post = req.body
  
  if (post.markdown) {
    post.html =  converter.makeHtml(post.markdown)
  }

  let result = {
    op: 'INS' 
  } 

  post.slug = slugify(post.title, {
    lower: true,      // convert to lower case, defaults to `false`
    strict: true
  })
  posts.insert(post, function (err) {
    if (err) {
      result.status = 'ERR'
      result.err = err
    } else {
      result.status = 'OK'
      result.id = this.lastID
    }
    res.json(result)
  })
})

module.exports = router
