var express = require('express');
const app = require('../app');
var router = express.Router();
const posts = require('../db/Posts');

/* GET home page. */
router.get('/', function (req, res) {
  posts.fetchAll(
    ['id', 'title', 'tags', 'status', 'slug'],
    process.env.DEFAULT_LANGUAGE,
    function (err, rows) {
      if (err) {
        res.render('partial/error.njk', { error: err })
      } else {
        const columns = ['id', 'title', 'html', 'tags', 'status']
        if (Number(req.cookies.currentId)) {
          posts.fetchByPK(columns, req.cookies.currentId, function (err, row) {
            if (err) {
              res.render('partial/error.njk', { error: err })
            } else {
              res.render('index.njk', { rows: rows, selected: row, selected_id: req.cookies.selectedId || row.id });
            }
          })
        } else {
          posts.fectchFirst(columns, function (err, row) {
            if (err) {
              res.render('partial/error.njk', { error: err })
            } else {
              res.render('index.njk', { rows: rows, selected: row, selected_id: req.cookies.selectedId || row.id});
            }
          }
          )
        }
      }
    }
  )
});

module.exports = router;
