var express = require('express');
const app = require('../app');
var router = express.Router();

/* GET home page. */
router.get('/:configvar', function(req, res, next) {
  res.set('Content-Type', 'text/plain')
  res.send(process.env[req.params.configvar])
});

module.exports = router;
