const db = require('../db/connection');

class Posts {
    static fectchFirst = (columns, callback) => {
        let cols = columns.map(c => { return `p1.${c}` })
        cols = cols.join(', ')

        const query = `
            SELECT
                ${cols}, 
                p1.translation_id,
                t.count AS translations_count,
                GROUP_CONCAT(IIF(p2.id IS NULL AND p2.language IS NULL, NULL, PRINTF('{"post_id": "%s", "translation_code": "%s"}', p2.id, p2.language)), '#') AS translations
            FROM posts p1
                LEFT JOIN posts p2 ON p2.translation_id = p1.translation_id AND p2.language <> $language
                INNER JOIN translations t ON t.id = p1.translation_id 
            ORDER BY p1.id
            LIMIT 1`

        const stmt = db.prepare(query)
        stmt.get({}, callback);
    }

    static fetchByPK = (columns, pkValue, callback) => {
        let cols = columns.map(c => { return `p1.${c}` })
        cols = cols.join(', ')

        const query = `
            SELECT 
                ${cols}, 
                p1.translation_id,
                p1.language,
                t.count AS translations_count,
                GROUP_CONCAT(IIF(p2.id IS NULL AND p2.language IS NULL, NULL, PRINTF('{"post_id": "%s", "translation_code": "%s"}', p2.id, p2.language)), '#') AS translations
            FROM posts p1
                LEFT JOIN posts p2 ON p2.translation_id = p1.translation_id AND p2.language <> $language
                INNER JOIN translations t ON t.id = p1.translation_id 
            WHERE p1.id = $id
        `

        const stmt = db.prepare(query)
        stmt.get({ $id: pkValue }, callback)
    }

    static fetchAll = (columns, language, callback) => {
        let cols = columns.map(c => { return `p1.${c}` })
        cols = cols.join(', ')

        const query = `
            SELECT 
                ${cols},
                p1.translation_id,
                t.count AS translations_count, 
                GROUP_CONCAT(IIF(p2.id IS NULL AND p2.language IS NULL, NULL, PRINTF('{"post_id": "%s", "translation_code": "%s"}', p2.id, p2.language)), '#') AS translations
            FROM posts p1 
                LEFT JOIN posts p2 ON p2.translation_id = p1.translation_id AND p2.language <> $language
                INNER JOIN translations t ON t.id = p1.translation_id 
            WHERE p1.language = $language
            GROUP BY p1.translation_id
            ORDER BY p1.id DESC
        `
        const params = {
            $language: language.toString()
        }

        const stmt = db.prepare(query, params)
        stmt.all(callback)
    }

    static update = (post, callback) => {
        console.log(`Update post: ${post.id} on DB!`)
        
        let columns = new Array()
        let params = {}
        for (const [key, value] of Object.entries(post)) {
            if (key != 'id') {
                columns.push(`\n'${key}' = :${key}`)
                params[`:${key}`] = value.toString()
            }
        }

        const query = `UPDATE posts\nSET ${columns.join(', ')}\nWHERE id = ${post.id}`
        const stm = db.prepare(query, params)

        stm.run(callback)
    }

    static insert = (post, callback) => {
        console.log('Inserting new post on DB!')
        console.log(post)
        if (post.id > 0) {
            callback(`You are trying to INSERT an existing post: ${post.id} use UPDATE instead!`)
            return false
        }
        
        delete post.id

        if (!post.title) {
            callback('Post title is mandatory, INSERT is not allowed!')
            return false
        }

        let params = {}
        const columns = []
        for (const [key, value] of Object.entries(post)) {
            columns.push(key)
            params[`:${key}`] = value.toString()
        }

        const columnsName = columns.map(c => `'${c}'`).join(', ')
        const columnsBoundParam = columns.map(c => `:${c}`).join(', ')
        
        const query = `INSERT INTO posts(${columnsName}) VALUES (${columnsBoundParam})`
        const stm = db.prepare(query, params)

        stm.run(callback)
    }
}

module.exports = Posts;
