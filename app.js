var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var postRouter = require('./routes/post');
var configRouter = require('./routes/config');

var nunjucks = require('nunjucks');

require('dotenv').config();

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true, type: 'application/x-www-form-urlencoded' }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

const viewEnv = nunjucks.configure('views', {
  autoescape: true,
  express: app
});


const languages = process.env.LANGUAGES
  .split(',')
  .map(language => { 
      const l = language.split(':')
      return {
        "code": l[0],
        "name": l[1]
      }
    }
  )

//nunjucks.addFilter
viewEnv.addGlobal('languages', languages);
viewEnv.addFilter('json_parse', (json) => JSON.parse(json));

app.set('view engine', 'html');

// Routes
app.use('/', indexRouter);
app.use('/posts?', postRouter);
app.use('/config', configRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error.njk', {'res': res, 'err': err});
});


module.exports = app;
