clearInterval(typeof intervalId == 'undefined' ? 0 : intervalId)

// Copy HTML to clipboard
document.querySelector('#copyHTML').addEventListener('click', e => {
    let iconContainer = e.currentTarget.querySelector('.icon');
    navigator.clipboard.writeText(document.querySelector('#post-body').innerHTML.trim()).then(
        () => {
            iconContainer.innerHTML = icon('clipboard-check');
            setTimeout(
                () => {
                    iconContainer.innerHTML = icon('clipboard');
                },
                3000
            );
        },
        () => {
            iconContainer.innerHTML = icon('clipboard-x');
            setTimeout(
                () => {
                    iconContainer.innerHTML = icon('clipboard');
                },
                3000
            );
        }
    );
});

// Edit Post form
document.querySelector('#edit').addEventListener('click', event => {
    doFetch(`/posts/${event.currentTarget.dataset.id}/edit`)
})

// Fetch Post content via AJAX
document.querySelectorAll('.translate').forEach(el => {
    el.addEventListener('click', event => {
        doFetch(`/posts/${event.currentTarget.dataset.id}/translate`)
        event.stopImmediatePropagation()
        document.querySelector('.item-selected')?.classList.remove('item-selected')
        
        const closest = event.currentTarget.closest('.item')
        id = closest?.id
        closest?.classList.add('item-selected')

        document.cookie = `currentId=${id}`
        document.cookie = `selectedId=${id}`
    })
})

loadScript('https://cdn.jsdelivr.net/gh/highlightjs/cdn-release@11.7.0/build/highlight.min.js')
    .then(
        async () => {
            if (document.querySelector('.language-nginx')) {
                await loadScript('https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.7.0/languages/nginx.min.js')
            }
            hljs.highlightAll({ ignoreUnescapedHTML: true })
        }
    )

