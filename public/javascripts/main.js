/**
 * Loads a JavaScript file and returns a Promise for when it is loaded
 */
const loadScript = async (src, id = undefined) => {
    return new Promise((resolve, reject) => {
        src = String(src)
        id = id || src.substring(src.lastIndexOf('/') + 1)
        const oldScript = document.getElementById(id);
        const newScript = document.createElement('script')
        newScript.id = id
        newScript.type = 'text/javascript'
        newScript.onload = resolve
        newScript.onerror = reject
        newScript.src = src
        if ((oldScript === undefined) || (oldScript === null)) {
            document.body.appendChild(newScript)
        } else {
            document.body.replaceChild(newScript, oldScript)
        }
    })
}

let icon = (name, width = 16, height = 16, color = 'currentColor') => {
    return `
		<svg class="bi" width="${width}" height="${height}" fill="${color}">
	  		<use xlink:href="/icons/icons.svg#${name}"/>
		</svg>
	`
}

// Fetch Post content via AJAX
document.querySelectorAll('.item, .translation').forEach((el) => {
    el.addEventListener('click', (event) => {
        document.querySelector('.item-selected').classList.remove('item-selected')
        let id 
        if (event.currentTarget.classList.contains('translation')) {
            id = event.currentTarget.dataset.id
            const closest = event.currentTarget.closest('.item')
            selectedId = closest.id
            closest.classList.add('item-selected')
            event.stopImmediatePropagation()
        } else {
            event.currentTarget.classList.add('item-selected');
            id = event.currentTarget.id
            selectedId = id
        }

        document.cookie = `currentId=${id}`
        document.cookie = `selectedId=${selectedId}`
        fetch(`/posts/${id}/view`)
            .then(response => response.text())
            .then(data => {
                document.getElementById('main').innerHTML = data;
                loadScript('/javascripts/view.js', 'view.js');
            })
    })
})

const createObjectStore = async (transaction, objectStoreName) => {
    return new Promise((resolve, reject) => {
        try {
            const objectStore = transaction.objectStore(objectStoreName)
            resolve(objectStore)
        } catch (error) {
            reject(error)
        }
    })
}

const createTransaction = async (db, objectStoreName, mode = 'readonly') => {
    return new Promise((resolve, reject) => {
        try {
            const transaction = db.transaction(objectStoreName, mode)
            console.log(`Transaction in ${transaction.mode} mode has been created sucessfuly on database: ${transaction.db.name} and object store: ${objectStoreName}`)
            resolve(transaction)
        } catch (error) {
            reject(error)
        }
    })
}

const createDataBase = async (dbName, dbVersion = 1, onupgradeneeded = () => {}) => {
    return new Promise((resolve, reject) => {
        if (!dbName) {
            throw new Error('Param: dbName is mandatory!')
        }
        if (!window.indexedDB) {
            throw new Error('Your browser doesn\'t support a stable version of IndexedDB.')
        }
        const dbRequest = window.indexedDB.open(dbName, dbVersion)
        dbRequest.onsuccess = resolve
        dbRequest.onerror = reject
        dbRequest.onupgradeneeded = onupgradeneeded
    })
}

class DBQuery {
    static #exec = (dbObject, objectStoreName, document, method = 'get') => {
        return new Promise((resolve, reject) => {
            dbObject.then(event => {
                const db = event.target.result
                createTransaction(db, objectStoreName, method == 'get' ? 'readonly' : 'readwrite').then(transaction => {
                    createObjectStore(transaction, objectStoreName).then(objectStore => {
                        let req
                        switch (method) {
                            case 'put':
                                req = objectStore.put(document)
                                break;

                            default:
                                req = objectStore.get(document[objectStore.keyPath])
                                break;
                        }
                        req.onsuccess = resolve
                        req.onerror = reject
                    })
                    transaction.oncomplete = () => {
                        console.log('Transaction completed!')
                    }
                    transaction.onerror = () => {
                        console.log('Transaction error!')
                    }
                    transaction.onabort = () => {
                        console.log('Transaction aborted!')
                    }
                }).catch(error => reject(error))
            }).catch(error => reject(error))
        })
    }


    static get(documentDB, objectStoreName, document) {
        return DBQuery.#exec(documentDB, objectStoreName, document, 'get')
    }

    static put(documentDB, objectStoreName, document) {
        return DBQuery.#exec(documentDB, objectStoreName, document, 'put')
    }
}

class DBPosts extends DBQuery {
    static #dbName = 'posts'
    static #dbVersion = 1
    static #dbObject = undefined;

    static #create() {
        if (this.#dbObject == undefined) {
            this.#dbObject = createDataBase(this.#dbName, this.#dbVersion, event => { // On upgrade needed
                const db = event.target.result
                if (!db.objectStoreNames.contains('post')) {
                    db.createObjectStore('post', { keyPath: 'id' })
                }
            })
        }
    }

    static get(document) {
        if (this.#dbObject == undefined) {
            this.#create()
        }
        return super.get(this.#dbObject, 'post', document)
    }

    static put(document) {
        if (this.#dbObject == undefined) {
            this.#create()
        }
        return super.put(this.#dbObject, 'post', document)
    }
}

const doFetch = async endPoint => {
    fetch(endPoint)
    .then(response => response.text())
    .then(async data => {
        document.getElementById('main').innerHTML = data
        loadScript('https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.5/codemirror.min.js')
        .then(() => {
            loadScript('https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.5/mode/markdown/markdown.min.js')
            .then(() => {
                let editor = CodeMirror.fromTextArea(document.getElementById('editor'), { lineNumbers: true, mode: 'markdown', lineWrapping: true });
                editor.setSize('100%', '100%');
                editor.on('change', instance => { instance.save() })
            })
            .then(async () => {
                fetch('/config/AUTOSAVE_FREQ')
                .then(response => response.text())
                .then(autosaveFreq => {
                    if (autosaveFreq > 0) {
                        let formData = new FormData(document.getElementById('form'))
                        let data = {}
                        for (const [key, value] of formData) {
                            if (value) {
                                data[key] = value
                            }
                        }

                        if (!data?.id) {
                            data.id = '_'
                        }

                        DBPosts.put(data).then(() => console.log('Data saved OK on the browser local storage!'))

                        intervalId = setInterval(() => {
                                let formData = new FormData(document.getElementById('form'))
                                id = formData.get('id') || '_'

                                DBPosts.get({ id: id }).then((event) => {
                                    const savedContent = event.target.result

                                    let data = {}
                                    for (const [key, value] of formData) {
                                        if (value) {
                                            data[key] = value
                                        }
                                    }

                                    data.id = id

                                    if (JSON.stringify(data) != JSON.stringify(savedContent)) {
                                        console.log('Saving data!')
                                        const notificationElement = document.getElementById('notification')
                                        notificationElement.innerHTML = 'Saving...'
                                        DBPosts.put(data).then(() => {
                                            console.log('Data saved OK on the browser local storage!')

                                            const headers = new Headers({
                                                "Content-Type": "application/json"
                                            })

                                            const request = new Request('/posts', {
                                                headers: headers,
                                                method: id == '_' && 'POST' || 'PUT', // POST: add, PUT: update
                                                body: JSON.stringify(data)
                                            })

                                            fetch(request).then(response => response.text()).then(result => {
                                                console.log(result)
                                                result = JSON.parse(result)
                                                if (result.status == 'OK') {
                                                    notificationElement.innerHTML = 'Saved OK...'
                                                    if (result.op == 'INS') {
                                                        document.getElementById('form').querySelector('input[name="id"]').value = result.id
                                                        document.cookie = `currentId=${result.id}`
                                                        //document.cookie = `selectedId=${result.id}`
                                                    }
                                                } else if (result.status == 'ERR') {
                                                    notificationElement.innerHTML = response.err
                                                }
                                                setTimeout(() => { notificationElement.innerHTML = '' }, 4000)
                                            })
                                        }).catch(error => console.log(error))
                                    } else {
                                        console.log('Content are indentical nothing to save!')
                                    }

                                })
                                .catch(error => console.log(error))
                            },
                            autosaveFreq * 1000
                        )
                    }
                })
            })
        })
    })
    .then(() =>  document.getElementById('post').addEventListener('submit', (e) => { e.preventDefault() }))
}

// Edit Post form
document.querySelector('#new').addEventListener('click', event => {
    doFetch('/posts/new')
})